# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3


class BreakWithEvent(Event3):
    NAME = "break-with"

    def perform(self):
        self.inform("break-with")
        if not self.object.has_prop("breakable"):
            self.fail()
            return self.inform("break.failed")
        self.inform("break")
